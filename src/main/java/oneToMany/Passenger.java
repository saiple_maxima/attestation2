package oneToMany;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Passenger {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;
    //КОЛИЧЕСТВО ОБЪЕКТОВ ТЕКУЩЕГО КЛАССА К (To) КОЛИЧЕСТВУ ОБЪЕКТОВ ДРУГОГО КЛАССА
    @ManyToOne
    @JoinColumn(name = "airplane_id")
    private AirPlane airPlane;
}

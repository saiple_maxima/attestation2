package oneToMany;

import oneToOne.Passport;
import oneToOne.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Arrays;
import java.util.List;

public class OneToManyTest {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();

        configuration.configure();

        try (SessionFactory sessionFactory = configuration.buildSessionFactory()){
            Session session = sessionFactory.openSession();
//
//            AirPlane airPlane = AirPlane.builder()
//                    .name("Boeing 777")
//                    .build();
//
//            Passenger passenger1 = Passenger.builder()
//                    .firstName("Oleg")
//                    .lastName("Igonin")
//                    .airPlane(airPlane)
//                    .build();
//
//            Passenger passenger2 = Passenger.builder()
//                    .firstName("Booch")
//                    .lastName("Booch")
//                    .airPlane(airPlane)
//                    .build();
//
//            airPlane.setPassengers(Arrays.asList(passenger1, passenger2));
//            session.save(airPlane);
//            session.save(passenger1);
//            session.save(passenger2);
            AirPlane airPlane = session.get(AirPlane.class, 1L);
            System.out.println("А теперь обратимся к пассажирам");
            List<Passenger> passengers = airPlane.getPassengers();

            for (Passenger passenger : passengers) {
                System.out.println(passenger.getFirstName());
            }
            session.close();
        }
    }
}

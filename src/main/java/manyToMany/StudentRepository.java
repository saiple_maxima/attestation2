package manyToMany;

import java.util.List;

public interface StudentRepository {

    List<Student> getAll();

}
